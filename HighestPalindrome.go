package main

import "fmt"

func createPalindrome(first int, last int, k int, bytes []byte) bool {
	// fmt.Println("first index: ", first)
	// fmt.Println("last index: ", last)

	// base event
	if first >= last {
		return true
	}

	// process
	if bytes[first] != bytes[last] {
		if k == 0 {
			return false
		}
		if bytes[first] > bytes[last] {
			// fmt.Println("first: ", string(bytes[first]))
			// fmt.Println("last: ", string(bytes[last]))
			bytes[last] = bytes[first]
		} else {
			// fmt.Println("First: ", string(bytes[first]))
			// fmt.Println("Last: ", string(bytes[last]))
			bytes[first] = bytes[last]
		}
		k--
	}
	return createPalindrome(first+1, last-1, k, bytes)
}

func findPalindrome(s string, k int) string {
	n := len(s)
	if n == 0 {
		return "-1" // if there is no len then return -1
	}

	bytes := []byte(s)
	//println(bytes)

	// create palindrome
	result := createPalindrome(0, n-1, k, bytes)

	if result == false {
		return "-1"
	}

	return string(bytes)
}

func main() {
	fmt.Println(findPalindrome("1943", 2)) // Output: 3993
}
