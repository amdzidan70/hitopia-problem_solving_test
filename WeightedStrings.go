package main

import (
	"fmt"
)

func generateArrayFromString(s string) []int {
	//println("len s: ", len(s))

	beenProcessed := []byte{}
	numArray := []int{}
	charCount := make(map[byte]int)

	// Counts the number of occurrences of each character
	for i := 0; i < len(s); i++ {
		charCount[s[i]]++
		// /fmt.Println(charCount)
	}

	// generate value from given string (exp. a = 1, aa = 2)
	for i := 0; i < len(s); i++ {
		char := s[i]
		numValue := int(char - 'a' + 1)

		// if the character only appear one time
		if charCount[char] == 1 {
			numArray = append(numArray, numValue)
		} else if charCount[char] > 1 {
			// check wether the character has been processed or not
			found := false
			for _, elem := range beenProcessed {
				if elem == char {
					found = true
					break
				}
			}

			// if the character has not been processed
			if found == false {
				// Insert numValue ​​into numArray according to the number of occurrences of the character				for j := 0; j < charCount[char]; j++ {
				for j := 0; j < charCount[char]; j++ {
					beenProcessed = append(beenProcessed, char)
					numArray = append(numArray, numValue)
					numValue += (int(char - 'a' + 1)) // add numValue
				}
			} else {
				continue // if the character has been processed, we skip it
			}
		}
	}

	return numArray
}

func WeightedStrings(s string, queries []int) []string {
	weights := generateArrayFromString(s)
	//fmt.Println(weights)

	// slice Initialisation
	results := make([]string, len(queries))

	// check every value in queries
	// wether exist or not in weights
	for i, q := range queries {
		found := false
		for _, w := range weights {
			if q == w {
				results[i] = "Yes"
				found = true
				break
			} else if q != w {
				continue
			}
		}

		if found == false {
			results[i] = "No"
		}
	}

	return results
}

func main() {
	inputString := "abbcccd"
	queries := []int{1, 3, 9, 8}

	output := WeightedStrings(inputString, queries)
	fmt.Println(output)
}
