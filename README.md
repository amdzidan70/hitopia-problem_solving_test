# Time Complexity Analysis


1. In BalancedBracket, we use strings.ReplaceAll(str, " ", "") to remove all whitespaces from input string. This function has a time complexity O(n), where n is the length of str. O(n) means that as n grows bigger, the function will take more time to execute.

2. We have 3 counters initialisation (counterRound, counterSquare, counterCurly). This operation is O(1). O(1) denotes that algorithm's complexity remains constant, regardless of the input size.

3. Looping through each bracket type also has a time complexity O(n).

Combining these, we sum O(n) + O(1) + O(n) = O(n). Therefore, the BalancedBracket function has a time complexity of O(n).