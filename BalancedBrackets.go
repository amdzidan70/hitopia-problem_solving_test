package main

import (
	"fmt"
	"strings"
)

func BalancedBracket(str string) string {
	// replace all whitespaces
	str = strings.ReplaceAll(str, " ", "")

	// Initialisation for every bracket's counter
	counterRound := 0
	counterSquare := 0
	counterCurly := 0

	// looping and branching for every bracket type
	for _, bracket := range str {
		if bracket == '(' {
			counterRound += '('
			//fmt.Println(counterRound)
		} else if bracket == ')' {
			counterRound -= '('
			if counterRound < 0 {
				return "NO"
			}
		} else if bracket == '[' {
			counterSquare += '['
			//fmt.Println(counterSquare)
		} else if bracket == ']' {
			counterSquare -= '['
			if counterSquare < 0 {
				return "NO"
			}
		} else if bracket == '{' {
			counterCurly += '{'
			//fmt.Println(counterCurly)
		} else if bracket == '}' {
			counterCurly -= '{'
			if counterCurly < 0 {
				return "NO"
			}
		}
	}

	// if all bracket's counters equal to 0,
	// it means that the bracket is balanced
	if counterRound == 0 && counterSquare == 0 && counterCurly == 0 {
		return "YES"
	}

	return "NO"
}

func main() {
	var str string
	str = "(      ) (      ) (     )    {{{{}}}}    "
	//str = "()[]}"
	//str = ")()"
	result := BalancedBracket(str)
	fmt.Println(result)
}
